# cakephp-awsauth

CakePHP AWS Authentication Classes

## Summary

These classes are helper classes for using AWS-based authentication to various
AWS resources like RDS databases.

## Usage

Coming soon...

## Contact

[John Oberly III <johnoberly@yeshealth.com>](mailto:johnoberly@yeshealth.com)
